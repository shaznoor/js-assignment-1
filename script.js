(function(){

    const myTable=document.querySelector('#table');

    let employees=[
        {name:'Md. Shaz Hassan',age:22,dob:'25/10/1998',email:'shaz@eazydiner.com',company:'EazyDiner Pvt. Ltd.'},
        {name:'Prateek Aggarwal',age:22,dob:'17/07/1998',email:'prateek@eazydiner.com',company:'EazyDiner Pvt. Ltd.'},
        {name:'Sanchit Jain',age:21,dob:'25/02/1999',email:'sanchit@eazydiner.com',company:'EazyDiner Pvt. Ltd.'},
        {name:'Sai Eswar',age:22,dob:'12/12/1998',email:'sai@eazydiner.com',company:'EazyDiner Pvt. Ltd.'},
        {name:'Jaydeep',age:21,dob:'05/05/1999',email:'jaydeep@eazydiner.com',company:'EazyDiner Pvt. Ltd.'}
    ];

    let headers=['Name','Age','DOB','Email','Company'];
    
    let table=document.createElement('table');
    let headerRow=document.createElement('tr');
    
    headers.forEach(headerText => {
        let header=document.createElement('th');
        let textNode=document.createTextNode(headerText);
        header.appendChild(textNode);
        headerRow.appendChild(header);
    });

    table.appendChild(headerRow);

    employees.forEach(emp => {
        let row=document.createElement('tr');
        Object.values(emp).forEach(text => {
            let cell=document.createElement('td');
            let textNode=document.createTextNode(text);
            cell.appendChild(textNode);
            row.appendChild(cell);
        });
        table.appendChild(row);
    });

    myTable.appendChild(table);
}());
